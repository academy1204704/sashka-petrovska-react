import React from "react";

function MyFirstComponent() {
    return (
        <h2>Hello, World!</h2>
    );
}

export default MyFirstComponent;