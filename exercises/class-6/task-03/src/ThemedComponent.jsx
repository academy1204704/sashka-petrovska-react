import React, { useContext } from "react";

// Component consuming the theme context
function ThemedComponent() {
    const theme = useContext(ThemeContext);

    return <div style={{ 
        background: theme === 'light' ? "#FFF" : "#333" , 
        color: theme === 'light' ? "#333" : "#FFF"}}>Themed Component</div>;
}

export default ThemedComponent;