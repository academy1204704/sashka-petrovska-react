import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css';
import ComponentState from './components/ComponentState';
import Counter from './components/Counter';
import Toggle from './components/Toggle';
import ConditionalRendering from './components/ConditionalRendering';
import DynamicList from './components/DynamicList';
import MultiInputForm from './MultiInputForm';
import Header from './components/Header';
import TimerComponent from './components/TimerComponent';
import CheckNumber from './CheckNumber';
import AppContext from './AppContext';
import LanguageContext from './components/LanguageContext';
import AuthenticationContext from './components/AuthenticationContext';
import CartContext from './components/CartContent';
import CountRef from './components/CountRef';
import AccessDomRef from './components/AccessDomRef';
import CounterReducer from './components/CounterReducer';
import CountMemo from './components/CountMemo';
import OptimizingEventsCallback from './components/OptimizingEventsCallback';
import CoolButton from './components/CoolButton';

function App() {
  return (
    <>
      {/* <Counter />
      <br />

      <ComponentState />
      <br /> <br />

      <Toggle />
      <br /> <br />

      <ConditionalRendering />
      <br /> <br />

      <DynamicList />
      <br /> <br /> */}

      {/* <MultiInputForm />
      <br /> <br /> */}

      {/* <Header/> */}

      {/* <TimerComponent /> */}

      {/* <CheckNumber /> */}

      {/* <AppContext /> */}

      {/* <LanguageContext /> */}

      {/* <AuthenticationContext /> */}

      {/* <CartContext /> */}

      {/* <CountRef /> */}

      {/* <AccessDomRef /> */}

      {/* <CounterReducer /> */}

      {/* <CountMemo /> */}

      {/* <OptimizingEventsCallback /> */}

      <CoolButton />
    </>
  );
}

export default App
