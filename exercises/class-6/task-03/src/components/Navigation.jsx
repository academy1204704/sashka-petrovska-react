import React from 'react';

function Navigation({ classStyle }) {
    return (
        <nav className={`navigation ${classStyle}`}>
            <a href="#">Home</a>
            <a href="#">About</a>
            <a href="#">Contact</a>
        </nav >
    );
}

export default Navigation;