import React, { useImperativeHandle, useRef, forwardRef } from 'react';

const ChildComponent = forwardRef((props, ref) => {
    const buttonRef = useRef();

    useImperativeHandle(ref, () => ({
        handleClick: () => {
            buttonRef.current.click();
            // console.log("CLICK");
        }
    }));

    return (
        <button ref={buttonRef}>Click me</button>
    );
});

function CoolButton() {
    const myBtn = useRef();

    const handleClick = () => {
        myBtn.current.handleClick();
        console.log("CLICK");
    };

    return (
        <div>
            <ChildComponent ref={myBtn} />
            <button onClick={handleClick}>Trigger</button>
        </div>
    );
};

export default CoolButton;