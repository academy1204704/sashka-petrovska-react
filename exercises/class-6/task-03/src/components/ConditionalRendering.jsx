import { useState } from 'react'

function ConditionalRendering() {
    const [isVisible, setIsVisible] = useState(false);

    return (
        <div>
            <button onClick={() => setIsVisible(!isVisible)}>
                {isVisible ? "Hide" : "Show"}
            </button>

            {isVisible &&
                (<p>
                    This paragraph is {isVisible ? "visible" : "hidden"}.
                </p>)
            }
        </div>
    );
}

export default ConditionalRendering;