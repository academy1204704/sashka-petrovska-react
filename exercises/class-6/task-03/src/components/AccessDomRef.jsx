import React, { useRef, useEffect } from "react";

function AccessDomRef() {
    const inputRef = useRef(null);

    useEffect(() => {
        // Focus on the input element when the component mounts
        inputRef.current.focus();
    }, []);

    return (
        <div>
            <input type="text" ref={inputRef} />
        </div>
    );
}

export default AccessDomRef;