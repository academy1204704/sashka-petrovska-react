import React, { createContext, useContext } from 'react';

// Creating an element (context) for our language
const CreateLanguageContext = createContext();

// Component used as a language provider 
const LanguageProvider = ({ children }) => {
    const language = 'en';
    // Any abbreviation in the '' can be used for any language

    return (
        <CreateLanguageContext.Provider value={language}>
            {children}
        </CreateLanguageContext.Provider>
    );
};

// Component that will be consuming the language context
const LanguageComponent = () => {
    const language = useContext(CreateLanguageContext);

    const translations = {
        en: "Hello",
        fr: "Bonjour",
        es: "Hola"
    }

    return (
        <div>{translations[language]}</div>
    );
}

// Using language context
function LanguageContext() {
    return (
        <LanguageProvider>
            <LanguageComponent />
        </LanguageProvider>
    );
}

export default LanguageContext;