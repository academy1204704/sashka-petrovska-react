import React, { createContext, useContext, useState } from "react";

// Creating context for authentication
const AuthContext = createContext();

// Authentication provider component
function AuthProvider({ children }) {
    const [user, setUser] = useState(null);

    const login = (username, password) => {
        // Implementing authentication logic here (?)
        setUser({ username });
    };

    const logout = () => {
        // Implementing logout logic here (?)
        setUser(null);
    };

    return (
        <AuthContext.Provider value={{ user, login, logout }}>
            {children}
        </AuthContext.Provider>
    );
}

// Component that will be consuming the authentication context
function AuthComponent() {
    const { user, login, logout } = useContext(AuthContext);

    return (
        <div>

            {user ? (
                <div>
                    <p>Welcome, {user.username}!</p>

                    <button onClick={logout}>Logout</button>
                </div>
            ) : (
                <div>
                    <p>Please login.</p>

                    <button onClick={() => login('Jane Doe', 'password')}>Login</button>
                </div>
            )}

        </div>
    )
}

function AuthenticationContext() {
    return (
        <AuthProvider>
            <AuthComponent />
        </AuthProvider>
    );
};

export default AuthenticationContext;