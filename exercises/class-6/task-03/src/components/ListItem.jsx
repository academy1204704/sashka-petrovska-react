import React from 'react'

function ListItem(id, item) {
    return (
        <li index={id}>

            <h3>List
                <span>{item}</span>
            </h3>

        </li>
    );
}

export default ListItem;