import React, { useState, useCallback } from 'react';

function OptimizingEventsCallback() {
    const [text, setText] = useState('');

    // useCallback is used to memorize the event handler
    const handleChange = useCallback((e) => {
        setText(e.target.value);
    }, []);

    return (
        <div>
            <input type="text" value={text} onChange={handleChange} />
            <p>Text: {text}</p>
        </div>
    )
};

export default OptimizingEventsCallback;