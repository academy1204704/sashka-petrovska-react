import React, { useMemo, useState } from "react";

function ExpensiveCalculation({ value }) {
    const expensiveValue = useMemo(() => {

        // Simulating a costly computation
        let result = 0;

        for (let i = 0; i < value; i++) {
            result += i;
        }

        return result;

    }, [value]);
    // Recomputating 'expensiveValue' when the value changes

    return (
        <div>
            <p>Expensive value: {expensiveValue}</p>
        </div>
    );
}

function CountMemo() {
    const [count, setCount] = useState(0);

    return (
        <div>
            <button onClick={() => setCount(count + 1)}>Increment</button>
            <ExpensiveCalculation value={count} />
        </div>
    );
};

export default CountMemo;