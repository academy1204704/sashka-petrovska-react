import React, { useState } from 'react'

function Counter() {
    const [name, setName] = useState('Taylor');

    const [age, setAge] = useState(20);

    function handleNameChange(e) {
        setName(e.target.value);
    }

    function handleAgeChange() {
        setAge(age + 1);
    }

    return (
        <>
            <input type="text" value={name} onChange={handleNameChange} />
            <button onClick={handleAgeChange}>Add year</button>
            <p>
                Hello, {name}. You are {age} years old.
            </p>
        </>
    );
}

export default Counter;