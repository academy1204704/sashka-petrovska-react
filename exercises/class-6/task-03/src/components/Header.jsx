import React, { useState, useEffect } from "react";
import Navigation from "./Navigation";

function Header() {
    const [activeNav, setActiveNav] = useState(false);

    useEffect(() => {
        alert("Hello side effect!");
    }, [activeNav]);

    return (
        <header className="header">
            <a href="/" className="logo">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                    <ellipse cx="12.07" cy="7" rx="3" ry="1.71"></ellipse>
                    <path d="M12.07 22c4.48 0 8-2.2 8-5V7c0-2.8-3.52-5-8-5s-8 2.2-8 5v10c0 2.8 3.51 5 8 5zm0-18c3.53 0 6 1.58 6 3a2 2 0 0 1-.29.87c-.68 1-2.53 2-5 2.12h-1.39C8.88 9.83 7 8.89 6.35 7.84a2.16 2.16 0 0 1-.28-.76V7c0-1.42 2.46-3 6-3z"></path>
                </svg>
            </a>

            <button className="menu-btn" onClick={() => setActiveNav(!activeNav)}>
                <span className="line"></span>
                <span className="line"></span>
            </button>

            <Navigation classStyle={activeNav ? "active" : ""} />
        </header>
    );
}

export default Header;
