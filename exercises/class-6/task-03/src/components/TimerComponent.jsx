import React, { useState, useEffect } from "react";

function TimerComponent() {
    const [seconds, setSeconds] = useState(0);

    useEffect(() => {
        // Function to increment seconds every second
        const timer = setInterval(() => {
            setSeconds((prevSeconds) => prevSeconds + 1);
        }, 1000);

        // Cleanup function to clear the interval when component unmounts
        return () => {
            clearInterval(timer);
        };
    }, []); // Empty dependency array to run only once on component mount/refresh

    return (
        <div>
            <div>Timer: {seconds} seconds</div>
        </div>
    );
}

export default TimerComponent;
