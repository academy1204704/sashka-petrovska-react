import React, { createContext, useContext, useState } from 'react';

const CreateCartContext = createContext();

function CartProvider({ children }) {
    const [cart, setCart] = useState([]);

    const [number, setNumber] = useState(0);

    const [text, setText] = useState("");

    const addToCart = (item) => {
        setCart([...cart, item]);
    }

    const removeFromCart = (itemToRemove) => {
        setCart(cart.filter(item => item !== itemToRemove))
    }

    return (
        <CreateCartContext.Provider value={{ cart, number, text, setText, setNumber, addToCart, removeFromCart }}>
            {children}
        </CreateCartContext.Provider>
    );
}

function CartComponent() {
    const { cart, number, setNumber, text, setText, addToCart, removeFromCart } = useContext(CreateCartContext);

    const handleChange = () => {
        setText(e.target.value);
    }

    return (
        <div>
            <ul>
                {cart.map((item, index) => (
                    <li key={index}>{item}</li>
                ))}
            </ul>

            <button onClick={() => {
                addToCart(text);
                setNumber(number + 1);
            }}>
                Add To Cart
            </button>

            <input type="text" value={text} onChange={handleChange} />
        </div>
    );
}

function CartContext() {
    return (
        <CartProvider>
            <CartComponent />
        </CartProvider>
    );
}

export default CartContext;