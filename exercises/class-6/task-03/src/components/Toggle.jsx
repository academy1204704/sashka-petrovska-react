import React, { useState } from 'react'

function Toggle() {
    const [isOn, setIsOn] = useState(false);

    return (
        <div className={isOn ? "day" : "night"}>
            <p>Toggle: {isOn ? "ON" : "OFF"} </p>
            <button onClick={() => setIsOn(!isOn)}>Toggle</button>
        </div>
    )
}

export default Toggle;