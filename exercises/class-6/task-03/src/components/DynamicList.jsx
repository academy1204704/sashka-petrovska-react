import React, { useState } from 'react'
import ListItem from './ListItem';

function DynamicList() {
    const [items, setItems] = useState([]);

    const addItem = () => {
        setItems(
            [...items, `Item ${items.length + 1}`]
        );
    }

    console.log(items);

    return (
        <div>
            <button onClick={addItem}>Add Item</button>

            <ul>
                {items.map((item, index) => (
                    < li key={index} > {item}</li>
                ))}
            </ul>

            {/* {items.map((item, index) => (
                    <ListItem key={index} id={index} item={item} />
                ))} */}


            {/* Alternative {
                    forEach((element, key) => {})
                } */}
        </div >
    );
}

export default DynamicList