import React, { useRef } from 'react';

function CountRef() {
    const count = useRef(0);

    const countIncrement = () => {
        console.log(count.current++);
    }

    return <button onClick={countIncrement}>{count.current}</button>;
}

export default CountRef;