import React, { useState } from 'react';

function MultiInputForm() {
    const [formData, setFormData] = useState({
        username: "",
        email: "",
        password: ""
    });

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(formData);
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={formData.username}
                name='username'
                placeholder='Username'
                onChange={handleChange}
            />

            <br /> <br />

            <input
                type="email"
                value={formData.email}
                name='email'
                placeholder='Email'
                onChange={handleChange}
            />

            <br /> <br />

            <input
                type="password"
                value={formData.password}
                name='password'
                placeholder='Password'
                onChange={handleChange}
            />

            <br /> <br />

            <button type='submit'>Submit</button>
        </form>
    );
}

export default MultiInputForm;