import React, { useState, useEffect } from "react";

function CheckNumber() {
    const [count, setCount] = useState(0);

    const [isEven, setIsEven] = useState(false);

    useEffect(() => {
        // This effect runs whenever 'count' changes
        setIsEven(count % 2 === 0);
    }, [count]);
    // Dependency array with "count", so the effect runs whenever "count changes"

    return (
        <div>
            <p>Count: {count}</p>

            <p>{isEven ? "Even" : "Odd"}</p>

            <button onClick={() => setCount(prevCount => prevCount + 1)}>Increment</button>
        </div>
    );
}

export default CheckNumber;