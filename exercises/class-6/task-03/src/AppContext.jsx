import React, { createContext, useContext } from "react";
// import ThemeProvider from "./ThemeProvider";
// import ThemedComponent from "./ThemedComponent";

const ThemeContext = createContext();

//Theme provider component
function ThemeProvider({ children }) {
    const theme = "light";

    return (
        <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
    );
}

// Component consuming the theme context
function ThemedComponent() {
    const theme = useContext(ThemeContext);

    return (
        <div
            style={{
                background: theme === "light" ? "#FFF" : "#333",
                color: theme === "light" ? "#333" : "#FFF",
            }}
        >
            Themed Component
        </div>
    );
}

function AppContext() {
    return (
        <ThemeProvider>
            <ThemedComponent />
        </ThemeProvider>
    );
}

export default AppContext;