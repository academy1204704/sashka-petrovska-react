import React from "react";
import classes from "./styles/Card.module.scss";
import Button from "./Button";

function Card() {
    return <>
        <article className={classes.card}>
            <h3 className={classes.title}>Card Title</h3>

            <p className={classes.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores voluptates ad eum laborum!</p>

            <Button style={classes.readBtn} title={"Read more"} />
            {/* <button className={classes.readBtn}>Read more</button> */}
        </article>

        <article className={classes.card}>
            <h3 className={classes.title}>Card Title</h3>

            <p className={classes.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores voluptates ad eum laborum!</p>

            <div className={classes.btnContainer}>
                {/* <button className={classes.readBtn}>Read more</button> */}
                <Button style={classes.readBtn} title={"Read more"} />

                <Button style={classes.contactBtn} title={"Contact me"} />
                {/* <button className={classes.contactBtn}>Contact me</button> */}
            </div>
        </article>
    </>;
};

export default Card;