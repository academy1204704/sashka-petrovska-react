import React from "react";
import Input from "./Input";
import classes from "./styles/FormComponent.module.scss"

function FormComponent() {
    return (
        <form className={classes.formElement} action="">
            <h1 className={classes.title}>Form</h1>

            <label>Enter your name:</label>
            {/* <Input type={prop} filler={"Enter name here..."} /> */}
            <Input type={"text"} text={"Name..."} />

            <br />

            <label>Enter your email:</label>
            <Input type={"email"} text={"Email..."} />

            <br />

            <label>Enter your password:</label>
            <Input type={"password"} text={"Password..."} />

            <button>Submit</button>
        </form>
    );
}

export default FormComponent;