import React from "react";

function Button({ style, title }) {
    return <>
        <button className={style}>{title}</button>
    </>;
}

export default Button;