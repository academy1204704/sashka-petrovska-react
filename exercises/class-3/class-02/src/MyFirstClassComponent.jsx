import React, { Component } from "react";

class MyFirstClassComponent extends Component {
    render() {
        return (
            <div>
                <h2>This is my first class component~</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, praesentium.</p>
            </div>
        );
    };
}

export default MyFirstClassComponent;