import React from "react";
import ListItem from "./ListItem";

function List() {
    return (
        <div>
            <h2>Grocery List</h2>

            <ul>
                <ListItem name={"lettuce"} />
                <ListItem name={"ramen"} />
                <ListItem name={"olives"} />
                <ListItem name={"bread"} />
                <ListItem name={"milk"} />
            </ul>
        </div>
    );
}

export default List;