import React from 'react';

function Post({ id, author, date, body }) {
    return (
        <a href={id}>
            <div className='postCard'>
                <h3 className='author'>{author}</h3>

                <span className='date'>{date}</span>

                <p className='body'>{body}</p>
            </div>
        </a>
    );
}

export default Post;