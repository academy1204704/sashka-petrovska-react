import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import FunctionalComponent from "./FunctionalComponent";
import ClassComponent from './ClassComponent';
import List from './List';
import MyFirstClassComponent from './MyFirstClassComponent';
import Post from './Post';

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>

      <FunctionalComponent message={"Welcome to the World of React.js!"} />
      <ClassComponent />
      <List />
      <MyFirstClassComponent />

      <br />

      <Post
        id={0}
        author={"Ryan Holiday"}
        date={"17.05.2024"}
        body={"This is a post about Ryan Holiday."} />
    </>
  )
}

export default App
