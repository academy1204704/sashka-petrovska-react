import React, { Component } from "react";

class ClassComponent extends Component {
    render() {
        return (
            <div>
                <h1>Welcome to the world of React.js</h1>
            </div>
        );
    };
}

export default ClassComponent;