import React from "react";

function FunctionalComponent(props) {
    return (
        <div>
            <h1>{props.message}</h1>
        </div>
    );
}

export default FunctionalComponent;