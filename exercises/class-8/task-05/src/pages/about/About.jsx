import React from "react";
import Header from "../../components/Header";

function About() {
    return (<>
        <Header />
        <h1>Welcome to our About page!</h1>
    </>
    );
}

export default About;