import React from "react";
import { Link } from "react-router-dom";
import Header from "../../components/Header";
import blogData from "../../blogData"
import blogPosts from "../../blogData";

function BlogPosts() {
    return (<>
        <Header />

        <section>

            <ul>
                {blogPosts.map((blog) => (
                    <li>
                        <Link to={`/blog/${blog.id}`} key={blog.id}>{blog.title}</Link>
                    </li>
                ))}
                {/* 
                <li>
                    <Link to={""}>Post 2</Link>
                </li>

                <li>
                    <Link to={""}>Post 3</Link>
                </li> */}
            </ul>

        </section>
    </>);
}

export default BlogPosts