import React from "react";
import styles from "./Home.module.scss"
import Header from "../../components/Header";
import HeroSection from "../../components/HeroSection";

function Home() {
    return (<>
        <Header />
        <HeroSection />
    </>
    );
}

export default Home;