import React from "react";
import { useParams } from "react-router-dom";
import Header from "../../components/Header";
import blogPosts from "../../blogData";

function BlogPostPage() {
    const { postId } = useParams();
    const single = blogPosts.find((single) => single.id === parseInt(postId));

    if (!single) {
        return <h1>Post not found</h1>
    }

    return <>
        <Header />
        <section>
            <h1>{single.title}</h1>

            <p className="description">{single.description}</p>

            <span>{single.author}</span>
        </section>
    </>
}

export default BlogPostPage;