import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

// import App from './App.jsx'
import "./index.scss"
import Home from './pages/home/Home'
import NotFoundPage from './NotFoundPage'
import About from './pages/about/About'
import BlogPosts from './pages/blog/BlogPosts'
import BlogPostPage from './pages/single/BlogPostPage'

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <NotFoundPage />,
  },
  {
    path: "/about",
    element: <About />,
    errorElement: <NotFoundPage />
  },
  {
    path: "/blog",
    element: <BlogPosts />,
  },
  {
    path: "/blog/:blogid",
    element: <BlogPostPage />
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);