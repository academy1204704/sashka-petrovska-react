const blogPosts = [
    {
        id: 1,
        title: "Post page 1",
        description: "Post id #1",
        author: "Sashka",
    },
    {
        id: 2,
        title: "Post page 2",
        description: "Post id #2",
        author: "Sashka",
    },
    {
        id: 3,
        title: "Post page 3",
        description: "Post id #3",
        author: "Sashka",
    },
    {
        id: 4,
        title: "Post page 4",
        description: "Post id #4",
        author: "Sashka",
    }
];

export default blogPosts;