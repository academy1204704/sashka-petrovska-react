import React from "react";
import styles from "./styles/HeroSection.module.scss"

function HeroSection() {
    return <section>
        <h1 className="heading">Hero Section</h1>

        <p className={styles.description}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi delectus placeat ducimus sunt in nam quaerat ipsa consequatur ratione natus.</p>
    </section>
}

export default HeroSection;