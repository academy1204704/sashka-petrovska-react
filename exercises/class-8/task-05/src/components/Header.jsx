import React from "react"
import { Link } from 'react-router-dom'
import styles from './styles/Header.module.scss'
import Logo from '../assets/ghibli-logo.png'

function Header() {
    return <header className={styles.header}>
        <Link href="#" className={styles.logoLink}>
            <img src={Logo} alt="" />
        </Link>

        <nav className={styles.navbar}>

            <ul className={styles.navigationLinks}>
                <li className={styles.ListItem}>
                    <Link to="/" className={styles.link}>Home</Link>
                </li>

                <li className={styles.ListItem}>
                    <Link to="/about" className={styles.link}>About</Link>
                </li>

                <li className={styles.ListItem}>
                    <Link to="/blog" className={styles.link}>Blog</Link>
                </li>

                <li className={styles.ListItem}>
                    <Link href="#" className={styles.link}>Contact</Link>
                </li>
            </ul>

        </nav>

    </header>
}

export default Header;