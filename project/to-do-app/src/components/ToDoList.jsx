import React, { useState } from "react";
import "./ToDoList.css";

function ToDoList() {
    // Using useState to define some placeholder tasks and enable updating them with setTasks
    const [tasks, setTasks] = useState(["Make bed", "Do skincare", "Stretch"]);

    // Using useState for adding new tasks, setting the new task to automatically be considered an empty string
    const [newTask, setNewTask] = useState("");

    // Adding a function to set the input's value as the value/text for the new task
    function handleInputChange(event){
        setNewTask(event.target.value);
    }

    // Adding a function to add the new task in the input to the existing task array
    function addTask() {
        setTasks(tasks => [...tasks, newTask]);
    }

    // Adding a function that enables deleting that specific list item from the array (that's identified with the `index`) and replacing the old task array with a new one that excludes the deleted element
    function deleteTask(index) {
        const newTasks = [...tasks];
        newTasks.splice(index, 1);
        setTasks(newTasks);
    }

    // Adding a function for moving the specific task one place above in the order of the tasks array
    function moveTaskUp(index) {
        if(index > 0) {
            const newTasks = [...tasks];
            [newTasks[index - 1], newTasks[index]] = [newTasks[index], newTasks[index -1]];
            setTasks(newTasks);
        }
    }

    // Same function as above, but for moving the task one place down this time
    function moveTaskDown(index) {
        if(index < tasks.length -1) {
            const newTasks = [...tasks];
            [newTasks[index], newTasks[index + 1]] = [newTasks[index + 1], newTasks[index]];
            setTasks(newTasks);
        }
    }

    return (<div className="container">

        <h1>To Do List</h1>

        <div className="input-div">
            <input
                type="text"
                placeholder="Enter a task..."
                onChange={handleInputChange}/>

            <button
                className="add-btn"
                onClick={addTask}>
                    <svg width="1em" height="1em" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g id="Edit / Add_Plus">
                        <path id="Vector" d="M6 12H12M12 12H18M12 12V18M12 12V6" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </g>
                    </svg>
            </button>
        </div>

        <ol>
            {/* Using the map function to create a separate list item element for each item in the task array, the "task" represents the value of the item (which is used as the text content in the list item here), and the "index" represents their index in the tasks array */}
            {tasks.map((task, index) => 
            <li key={index}>
                <span className="task-text">
                    {task}
                </span>

                <button
                    className="delete-btn"
                    onClick={() => deleteTask(index)}>
                    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18 6L17.1991 18.0129C17.129 19.065 17.0939 19.5911 16.8667 19.99C16.6666 20.3412 16.3648 20.6235 16.0011 20.7998C15.588 21 15.0607 21 14.0062 21H9.99377C8.93927 21 8.41202 21 7.99889 20.7998C7.63517 20.6235 7.33339 20.3412 7.13332 19.99C6.90607 19.5911 6.871 19.065 6.80086 18.0129L6 6M4 6H20M16 6L15.7294 5.18807C15.4671 4.40125 15.3359 4.00784 15.0927 3.71698C14.8779 3.46013 14.6021 3.26132 14.2905 3.13878C13.9376 3 13.523 3 12.6936 3H11.3064C10.477 3 10.0624 3 9.70951 3.13878C9.39792 3.26132 9.12208 3.46013 8.90729 3.71698C8.66405 4.00784 8.53292 4.40125 8.27064 5.18807L8 6" stroke="#ffffff" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </button>
                
                <button
                    className="move-btn"
                    onClick={() => moveTaskUp(index)}>
                    <svg width="1em" height="1em"  viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 6V18M12 6L7 11M12 6L17 11" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </button>

                <button
                    className="move-btn"
                    onClick={() => moveTaskDown(index)}>
                    <svg width="1em" height="1em" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12 6V18M12 18L7 13M12 18L17 13" stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </button>
            </li>)}
        </ol>

    </div>);
}

export default ToDoList;