import React, { useState } from "react";

function ConditionalRendering() {
    // Calling useState function at the start with state and setState, with the default value being false
    const [state, setState] = useState(false);

    return <div>
        {/* Creating a button here which will change the set the value of the state to be opposite of what it is currently */}
        {/* Also setting a conditional so that is the state has the value of false, button text will be "log in", and if it's true, text will be "log out" */}
        <button onClick={
            () =>
                setState(!state)}>
            {state ? "Log in" : "Log out"}
        </button>

        {/* Using the same method for the button text here to manipulate the paragraph's content */}
        <p> {state ? "Please log in first." : "Welcome, user!"} </p>
    </div>
}

export default ConditionalRendering;