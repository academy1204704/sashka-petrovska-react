import React from "react";
import ConditionalRendering from "./components/ConditionalRendering";

function App() {
  return (
    <>
      <ConditionalRendering />
    </>
  )
}

export default App;