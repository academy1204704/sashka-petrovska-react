import React, { useState } from 'react';
import classes from './ColorPicker.module.css'

function ColorPicker() {
    // Calling use state function here to change the text's color
    const [selectedColor, setSelectedColor] = useState('white');

    return <div className={classes.colorContainer}>
        {/* Creating 3 buttons, each of which will be used to change (set) the selected color to red, green and blue */}
        <div className={classes.buttonsContainer}>
            <button onClick={
                () =>
                    setSelectedColor('red')
            }>Red</button>

            <button onClick={
                () =>
                    setSelectedColor('green')
            }>Green</button>

            <button onClick={
                () =>
                    setSelectedColor('blue')
            }>Blue</button>
        </div>

        {/* Creating a paragraph that includes a span, which we will use for the exercise */}
        {/* Accessing the span text's color through its' style, and setting its' value to our set color from our useState function */}
        <p>Selected color (
            <span style={{ color: selectedColor }}>
                {selectedColor}
            </span>
            ).
        </p>
    </div >;
}

export default ColorPicker;